
-- BASE DE DATOS DEL PROYECTO DE SA DICIEMBRE
create database proyecto_sa_diciembre;

use proyecto_sa_diciembre;

create table departamento(
  codigo_departamento varchar(2) primary key,
  nombre_departamento varchar(100)
);

create table municipio(
  id_municipio int auto_increment primary key,
  codigo_municipio varchar(2),
  nombre_municipio varchar(100),
  id_departamento varchar(2),
  foreign key(id_departamento) references departamento(codigo_departamento)
);

CREATE TABLE dpi (
  cui varchar(13) primary key,
  Correlativo varchar(8),
  Digito_Validacion varchar(1),
  id_municipio int,
  -- Departamento varchar(2),
  Nombre varchar(200),
  apellido varchar(200),
  Genero varchar(200),
  Fecha_nac date,
  foreign key(id_municipio) references municipio(id_municipio)
);

-- alter table dpi auto_increment=10000000;

CREATE TABLE Acta_matrimonio (
  Id_acta int auto_increment primary key,
  DPI_esposo varchar(13),
  DPI_esposa varchar(13),
  -- Fecha_inicio date,
  Fecha_inicio varchar(13),
  Estado int,
  foreign key(DPI_esposo) references dpi(cui),
  foreign key(DPI_esposa) references dpi(cui)
);

CREATE TABLE Divorcio (
  Id_divorsio int primary key auto_increment,
  fecha_divorsio date,
  Id_acta_matrimonio int,
  foreign key(Id_acta_matrimonio) references Acta_matrimonio(Id_acta) 
);

CREATE TABLE Defuncion(
  id_defuncion int primary key auto_increment,
  dpi_difunto varchar(13),
  Fecha date,
  Causa_desceso varchar(500),
  foreign key(dpi_difunto) references dpi(cui)
);

CREATE TABLE Licencia (
  Id_licencia int primary key auto_increment,
  tipo_licencia char,
  antiguedad int,
  Fecha_Emision date,
  dpi varchar(13),
  foreign key(dpi) references dpi(cui)
);

CREATE TABLE Partida_Nacimiento (
  id_acta_naciento int auto_increment primary key,
  dpi_padre varchar(13),
  dpi_madre varchar(13),
  FechaNacimiento date,
  Nombre varchar(200),
  Apellido varchar(200),
  Genero char,
  -- Departamento varchar(2),
  id_municipio int,
  foreign key(dpi_padre) references dpi(cui),
  foreign key(dpi_madre) references dpi(cui),
  foreign key(id_municipio) references municipio(id_municipio)
);

delimiter ;;

create procedure generarActaMatrimonio(_dpi_esposo varchar(13), _pdi_esposa varchar(13), _fecha date)
begin 
	insert into acta_matrimonio(DPI_esposo,DPI_esposa,Fecha_inicio,Estado) values(_dpi_esposo,_pdi_esposa,_fecha,1);
end ;;

delimiter ;

insert into departamento values('01','Guatemala');
insert into departamento values('22','Jutiapa');

insert into municipio(codigo_municipio,nombre_municipio,id_departamento) values('01','Guatemala','01');
insert into municipio(codigo_municipio,nombre_municipio,id_departamento) values('15','Villa Nueva','01');
insert into municipio(codigo_municipio,nombre_municipio,id_departamento) values('14','Moyuta','22');


insert into proyecto_sa_diciembre.dpi values('2879708922214','28797089','2',1,'Juan','Solares','M','1995-10-30');
insert into proyecto_sa_diciembre.dpi values('2879458522214','28794585','2',1,'Maria','Garcia','F','1995-10-30');




-- PROCEDIMIENTO ALMACENADO PARA INSERTAR UNA NUEVA ACTA DE MATRIMONIO
DELIMITER ;;
CREATE DEFINER='root'@'%' PROCEDURE insertar_acta_matrimonio(
dpihombre varchar(13), dpimujer varchar(13), fecha varchar(13)
)
BEGIN
-- DECLARE EXIT HANDLER FOR SQLEXCEPTION
--	  BEGIN
--		select 401 estado,'Error al insertar acta de matrimonio' mensaje;               
--	  END;

-- Estado = 1 = acta activa
-- Estado = 0 = acta inactiva
-- insert into Acta_matrimonio (DPI_esposo, DPI_esposa, Fecha_inicio, Estado)
-- values(dpihombre,dpimujer, fecha, 1);

insert into Acta_matrimonio (DPI_esposo, DPI_esposa, Fecha_inicio, Estado)
values(dpihombre,dpimujer, fecha, 1);
  
select 200 estado, 'Acta de matrimonio insertada correctamente' mensaje;
END ;;
DELIMITER ;

